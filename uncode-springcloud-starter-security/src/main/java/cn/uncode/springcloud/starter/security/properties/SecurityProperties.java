package cn.uncode.springcloud.starter.security.properties;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

/**
 * jwt相关配置
 * 
 * @author juny
 * @date 2019年4月24日
 *
 */
@Data
@ConfigurationProperties(prefix = "info.app", ignoreInvalidFields = true)
public class SecurityProperties {

   private List<String> whiteUrlList;

}
