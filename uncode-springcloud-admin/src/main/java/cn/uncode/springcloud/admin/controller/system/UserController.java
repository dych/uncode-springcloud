package cn.uncode.springcloud.admin.controller.system;


import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Range;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.csp.sentinel.annotation.SentinelResource;

import cn.uncode.springcloud.admin.model.ValidateBean;
import cn.uncode.springcloud.starter.security.auth.EmbedUser;
import cn.uncode.springcloud.starter.web.exception.ExceptionUtils;
import cn.uncode.springcloud.starter.web.result.R;
import cn.uncode.springcloud.starter.web.result.ResultCode;
import cn.uncode.springcloud.utils.json.JsonUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;


/**
 * 控制器 demo
 * @author juny
 * @date 2019年1月31日
 *
 */
@RestController
@RequestMapping("/user")
@Slf4j
@Validated
public class UserController {


	/**
	 * 查询单条
	 */
	@ApiOperation(value = "查看详情", notes = "传入id", position = 1)
	@GetMapping("/detail")
	public R<EmbedUser> detail() {
		EmbedUser detail = new EmbedUser();
		detail.setLoginName("juny");
		detail.setUserId(1000001);
		detail.setRoleName("管理员");
		return R.success(detail);
	}

    @RequestMapping("/validate")
    public R<ValidateBean> validateDemo(@RequestBody ValidateBean validateBean) {
        return R.success(validateBean);
    }


    @RequestMapping("/logSensitive")
    @SentinelResource("resource")
    public R<ValidateBean> validateDemo() {
        ValidateBean validateBean = new ValidateBean();
        validateBean.setRealName("测试脱敏");
        validateBean.setCardId("314221144512100120");
        log.error(JsonUtil.toJson(validateBean));
        log.info(JsonUtil.toJson(validateBean));
        log.debug(JsonUtil.toJson(validateBean));
        return R.success("请求成功");
    }

    @GetMapping(value = "/hello")
    @SentinelResource("hello")
    public String hello() {
        return "Hello Sentinel";
    }
    
    @GetMapping(value = "/ex")
    public R<String> exception(@Range(min = 1, max = 9, message = "年级只能从1-9")int age) {
    	ExceptionUtils.throwException(ResultCode.PARAM_MISS);
        return R.success("请求成功");
    }
    
    @GetMapping(value = "/ex2")
    public R<String> exception2( @Pattern(regexp ="^\\d{4}$" ,message = "验证码不合法！")String no) {
        return R.success("请求成功");
    }
    
    
    
    
}
